<?php


namespace App;


class Hat {

    private const FILE = '/tmp/hat.json';

    private $inside;
    private $outside;

    public function __construct(array $inHat, array $outOfHat) {
        $this->inside = $inHat;
        $this->outside = $outOfHat;
    }

    public function getInside(): array {
        return $this->inside;
    }

    public function getOutside(): array {
        return $this->outside;
    }

    public function save(): void {
        file_put_contents(self::FILE, json_encode(['inside' => $this->inside, 'outside' => $this->outside]));
    }

    public static function load(): self {
        if (file_exists(self::FILE)) {
            $val = json_decode(file_get_contents(self::FILE), true);
            return new self(
                $val['inside'],
                $val['outside']
            );
        }

        return new self([], []);
    }

    public function add(string $value): self {
        $this->inside[] = $value;

        return $this;
    }

    public function draw() {
        if (count($this->inside) === 0) {
            return null;
        }

        shuffle($this->inside);
        $value = array_pop($this->inside);
        $this->outside[] = $value;

        return $value;
    }

    public function clear(): self {
        $this->inside = [];
        $this->outside = [];

        return $this;
    }

    public function fill(): Hat {
        $this->inside = array_merge($this->inside, $this->outside);
        $this->outside = [];
        return $this;
    }

}