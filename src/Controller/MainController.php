<?php

namespace App\Controller;

use App\Hat;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController {

    private $hat;

    public function __construct() {
        $this->hat = Hat::load();
    }

    /**
     * @Route("/", name="main", methods={"get"})
     */
    public function index(): Response {
        return $this->render('main/index.html.twig', [
            'hat' => $this->hat,
        ]);
    }

    /**
     * @Route("/add", name="add", methods={"post"})
     * @param Request $request
     * @return RedirectResponse
     */
    public function add(Request $request): RedirectResponse {
        $this->hat->add($request->get('new-value'))->save();

        return $this->redirect('/');
    }

    /**
     * @Route("/draw", name="draw", methods={"post"})
     * @return RedirectResponse
     */
    public function draw(): Response {
        $value = $this->hat->draw();
        $this->hat->save();

        return $this->render('main/draw.html.twig', [
            'hat' => $this->hat,
            'drawn' => $value,
        ]);
    }

    /**
     * @Route("/clear", name="clear", methods={"post"})
     * @return RedirectResponse
     */
    public function clear(): Response {
        $this->hat->clear()->save();

        return $this->redirect('/');
    }

    /**
     * @Route("/fill", name="fill", methods={"post"})
     * @return RedirectResponse
     */
    public function fill(): Response {
        $this->hat->fill()->save();

        return $this->redirect('/');
    }
}
